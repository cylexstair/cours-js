function additionner(a, b) {
    return a + b;
}

function soustraire(a, b) {
    return a - b;
}

function multiplier(a, b) {
    return a * b;
}

function diviser(a, b) {
    return a / b;
}

const prompt = require('prompt-sync')();//nécessaire pour input utilisateurs

const chiffre1 = prompt('Quel est le premier chiffre que vous souhaitez manipulez ? ');
if (isNaN(chiffre1) == true)    // vérifie que la valeur donné par l'utilisateur est un nombre
{
    console.log("\"" + chiffre1 + "\"" + " n'est pas un nombre valide (en tous cas d'après JS :])");
    process.exit();
}
const symbole = prompt('Quel calcul souhaitez vous appliquez ? ( + , - , * , / ) ');
const chiffre2 = prompt('Quel est le second chiffre que vous souhaitez manipulez ? ');
if (isNaN(chiffre2) == true)    // vérifie que la valeur donné par l'utilisateur est un nombre
{
    console.log("\"" + chiffre2 + "\"" + " n'est pas un nombre valide (en tous cas d'après JS :])");
    process.exit();
}
switch (symbole) {
    case '+':
        console.log(chiffre1 + " + " + chiffre2 + " = " + additionner(chiffre1, chiffre2));
        break;
    case '-':
        console.log(chiffre1 + " - " + chiffre2 + " = " + soustraire(chiffre1, chiffre2));
        break;
    case '*':
        console.log(chiffre1 + " x " + chiffre2 + " = " + multiplier(chiffre1, chiffre2));
        break;
    case '/':
        console.log(chiffre1 + " / " + chiffre2 + " = " + diviser(chiffre1, chiffre2));
        break;
    default :
        console.log("\"" + symbole + "\"" + " n'est pas un symbole valide :(");
        break;
}